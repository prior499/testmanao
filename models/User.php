<?php

define("XML_DIR", $_SERVER['DOCUMENT_ROOT'].'/testmanao/db/users.xml');

if (isset($_POST['reg']))
    User::act_Reg($_POST['login'], $_POST['password'], $_POST['confirm_password'], $_POST['email'], $_POST['name']);
elseif (isset($_POST['auth']))
    User::act_Auth($_POST['login'], $_POST['password']);

class User {

    public static function act_Reg($login, $password, $confirm_pass, $email, $name) {
        $errors = [];
        $status = false;
        if(empty($login) || empty($password) || empty($confirm_pass) || empty($email) || empty($name))
            $errors[] = "Fill all field";
        if(empty($errors)) {
            if (!User::checkLogin($login))
                $errors[] = "Login must contain only numbers and latin letters";
            if (!User::checkPass($password,
                $confirm_pass)) $errors[] = "Password do not match";
            if (!User::checkEmail($email))
                $errors[] = "Check right 'Email' field";
            if (!User::checkName($name))
                $errors[] = "Name must contain only latin letters";
            if(empty($errors)) {
                if (User::checkUniqLogin($login)) $errors[] = "Login already exists";
                if (User::checkUniqEmail($email)) $errors[] = "Email already exists";
                if (empty($errors)){
                    $hash_pass = User::HashPassword($password);
                    if (User::Register($login, $hash_pass, $email, $name)) $status = true;
                }
            }
        }
        echo json_encode(array($errors, $status));
        exit;
    }
    public static function act_Auth($login, $password, $sAuth = false) {
        $error = false;
        $status = false;
        if(empty($login) || empty($password) || !empty($sAuth))
            $error = "Fill all field";
        if(empty($error) || !empty($sAuth)) {
            if (User::checkAuth($login, $password) || !empty($sAuth)) {
                $sxml = simplexml_load_file(XML_DIR);
                foreach ($sxml->user as $user) {
                    if($login == $user->login || $sAuth === $user->sAuth) {
                        session_start();
                        $_SESSION['Name'] = strval($user->name);
                        $sAuthGen = User::HashPassword($user->password);
                        setcookie("sAuth", $sAuthGen, time() + 604800, '/');
                        $user->sAuth = $sAuthGen;
                        $sxml->asXML(XML_DIR);
                        $status = true;
                        break;
                    }
                }
            } else {
                $error = "Login or password do not much";
            }
        }
        echo json_encode(array($error, $status));
    }

    public static function act_Logout($sAuth) {
        $sxml = simplexml_load_file(XML_DIR);
        foreach ($sxml->user as $user) {
            if ($sAuth == $user->sAuth) {
                $user->sAuth = "";
                $sxml->asXML(XML_DIR);
                $_SESSION = array();
                session_destroy();
                setcookie('sAuth','', time()-10, '/');
                break;
            }
        }
    }
    public static function act_sessAuth($sAuth) {
            if(User::checkAuth_sessAuth($sAuth)) {
                User::act_Auth(false, false, $sAuth);
            } else {
                $_SESSION = array();
                session_destroy();
                setcookie('sAuth','', time()-10, '/');
            }
    }

    public static function checkLogin($login) {
        if (ctype_alnum($login)) {
            return true;
        } else {
            return false;
        }
    }
    public static function checkPass($password, $confirm_pass) {
        if ($password === $confirm_pass) {
            return true;
        } else {
            return false;
        }
    }
    public static function checkEmail($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }
    public static function checkName($name) {
        if (ctype_alpha($name)) {
            return true;
        } else {
            return false;
        }
    }
    public static function checkUniqLogin($login) {
        $sxml = simplexml_load_file(XML_DIR);
        $match = false;
        foreach ($sxml->user as $user) {
            if($login == $user->login) {
                $match = true;
                break;
            }
        }
        return $match;
    }
    public static function checkUniqEmail($email) {
        $sxml = simplexml_load_file(XML_DIR);
        $match = false;
        foreach ($sxml->user as $user) {
            if($email == $user->email) {
                $match = true;
                break;
            }
        }
        return $match;
    }
    public static function HashPassword($password) {
        $salt_hash = 'salt';
        $hash_pass = md5($salt_hash . $password);
        return $hash_pass;
    }
    public static function Register($login, $password, $email, $name) {

        $xml=new DomDocument('1.0','utf-8');
        if(!file_exists(XML_DIR)) {
            $xml->appendChild($xml->createElement('users'));
        }
        $xml->load(XML_DIR);
        $users = $xml->documentElement;
        $user = $users->appendChild($xml->createElement('user'));

        $user->appendChild($xml->createElement('sAuth', false));
        $user->appendChild($xml->createElement('login', $login));
        $user->appendChild($xml->createElement('password', $password));
        $user->appendChild($xml->createElement('email', $email));
        $user->appendChild($xml->createElement('name', $name));
//
//        $user_id = $user->appendChild($xml->createAttribute('id'));
//        $user_id->value = $login;

        $xml->save(XML_DIR);
        return true;
    }
    public static function checkAuth($login, $password) {
        $sxml = simplexml_load_file(XML_DIR);
        $match = false;
        foreach ($sxml->user as $user) {
            if($login == $user->login) {
                $input_pass = User::HashPassword($password);
                if($user->password == $input_pass) {
                    $match = true;
                }
                break;
            }
        }
        return $match;
    }
    public static function checkAuth_sessAuth($sAuth) {
        $sxml = simplexml_load_file(XML_DIR);
        $match = false;
        foreach ($sxml->user as $user) {
            if($sAuth == $user->sAuth) {
                $match = true;
                break;
            }
        }
        return $match;
    }
}