<?php


include './models/User.php';

if(isset($_COOKIE['sAuth']) && empty($_SESSION)) {
    User::act_sessAuth($_COOKIE['sAuth']);
}

class UserController
{
    public static function act_User()
    {

        if (empty($_SESSION)) {
            if (isset($_GET['act'])) {
                if ($_GET['act'] == "auth") {
                    require_once('views/auth.html');
                } else if ($_GET['act'] == "reg") {
                    require_once('views/reg.html');
                } else {
                    require_once('views/auth.html');
                }
            } else {
                require_once('views/auth.html');
            }
        } else {
            if ($_GET['act'] == "logout") {
                User::act_Logout($_COOKIE['sAuth']);
                require_once('views/auth.html');
            } else {
                require_once('views/main.html');
                $hello = 'World';
                if (isset($_SESSION['Name']))
                    $hello = $_SESSION['Name'];
                echo "<h1 class='display-4'>Hello," . $hello . "</h1>";
            }
        }
    }
}

require_once('views/head.html');

UserController::act_User();

require_once('views/footer.html');