$(document).ready(function(){
    $("[act='submit-reg']").click(function(){
        var dataReg = $('#form-reg').serialize();
        $.ajax({
            type: "POST",
            url: "./models/User.php",
            data: dataReg,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj[1]) {
                    alert('Registration Successful');
                    location.href = '?act=auth';
                } else {
                    var errmsg = '';
                    $.each(obj[0],function(i,value){
                        errmsg += '- ' + value + '\r\n';
                    });
                    alert(errmsg);
                }
            }
        });
    });
    $("[act='submit-auth']").click(function(){
        var dataAuth = $('#form-auth').serialize();
        $.ajax({
            type: "POST",
            url: "./models/User.php",
            data: dataAuth,
            success: function(data){
                var obj = $.parseJSON(data);
                console.log(obj);
                if(obj[1]) {
                    alert('Login Successful');
                    location.href = '?act=auth';
                } else {
                    alert('- ' + obj[0]);
                }
            }
        });
    });
});